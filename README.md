# Frost PowerShell Module

`Frost` is a PowerShell module designed to automate the setup of (virtual) Windows machines to provide a consistent reproducible development environment.

## Features

1. **PowerShell Upgrade**: Installs newer PowerShell version if available.
2. **Installs [Chocolatey](https://chocolatey.org/)**
3. **Basic Programs**: Installs essential programs like git, Google Chrome, gsudo, msys2, and Visual Studio Code using Chocolatey
4. **Install [Wedge](https://github.com/MarcGuiselin/wedge)**
5. **Install 1Password**
6. **WSL & Ubuntu Installation**: Checks and installs Windows Subsystem for Linux (WSL) and Ubuntu.
7. **Windows Terminal Configuration**: Applies specific Windows Terminal settings.
8. **Reboot**: Asks for confirmation to reboot the computer after installs.

## Usage

The module performs all provisioning actions if no parameters have been specified.

``` powershell
Invoke-Expression (Invoke-WebRequest -Uri 'https://gitlab.com/deposition.cloud/infra/compute/frost/-/raw/main/Frost.psm1').Content

Frost -Help
```

## Development

Load the PowerShell module for local development with:

```powershell
git clone https://gitlab.com/deposition.cloud/infra/compute/frost.git
cd frost
Set-ExecutionPolicy Bypass -Scope Process -Force
Remove-Module -Name Frost; Import-Module .\Frost.psm1; Frost -Help
```
