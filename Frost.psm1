new-module -name Frost -scriptblock {
    function Frost {
        param (
            [Parameter(Mandatory=$false)][switch]$Help,
            [Parameter(Mandatory=$false)][switch]$UpgradePowerShell,
            [Parameter(Mandatory=$false)][switch]$InstallChocolatey,
            [Parameter(Mandatory=$false)][switch]$InstallPrograms,
            [Parameter(Mandatory=$false)][switch]$InstallWedge,
            [Parameter(Mandatory=$false)][switch]$Install1Password,
            [Parameter(Mandatory=$false)][switch]$InstallWSL,
            [Parameter(Mandatory=$false)][switch]$InstallUbuntu,
            [Parameter(Mandatory=$false)][switch]$SetWSLUser,
            [Parameter(Mandatory=$false)][switch]$InstallWSLAnsible,
            [Parameter(Mandatory=$false)][switch]$InstallWinRM,
            [Parameter(Mandatory=$false)][switch]$ConfigureWindowsTerminal,
            [Parameter(Mandatory=$false)][switch]$Reboot,
            [Parameter(Mandatory=$false)][switch]$NoReboot
        )

        # Check for help request and display help information
        if ($Help) {
            Write-Host "Available parameters:"
            Write-Host "  -Help                      Show this help message and exit." -ForegroundColor DarkGray
            Write-Host "  -UpgradePowerShell         Upgrade PowerShell to the latest version if not at the latest." -ForegroundColor DarkGray
            Write-Host "  -InstallChocolatey         Install Chocolatey package manager." -ForegroundColor DarkGray
            Write-Host "  -InstallPrograms           Install basic programs (git, googlechrome, gsudo, msys2, vscode)." -ForegroundColor DarkGray
            Write-Host "  -InstallWedge              Install Wedge https://github.com/MarcGuiselin/wedge" -ForegroundColor DarkGray
            Write-Host "  -Install1Password          Install 1Password" -ForegroundColor DarkGray
            Write-Host "  -InstallWSL                Install Windows Subsystem for Linux (WSL)." -ForegroundColor DarkGray
            Write-Host "  -InstallUbuntu             Install Ubuntu on WSL." -ForegroundColor DarkGray
            Write-Host "  -ConfigureWindowsTerminal  Configure Windows Terminal settings." -ForegroundColor DarkGray
            Write-Host "  -Reboot                    Reboot the computer after installations." -ForegroundColor DarkGray
            Write-Host "  -NoReboot                  Do not reboot the computer automatically." -ForegroundColor DarkGray
            Write-Host ""
            return
        }

        # Check if running as Administrator
        if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
            Write-Host "This script requires to be run as Administrator with the execution policy set to Bypass.`n" -ForegroundColor Red
            Write-Host "Please open PowerShell as Administrator and run the following command before executing this script:"
            Write-Host "Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass" -ForegroundColor Green
            return
        }
        try {
            $latestReleaseInfo = Invoke-RestMethod -Uri "https://api.github.com/repos/PowerShell/PowerShell/releases/latest" -ErrorAction Stop
            $latestVersion = $latestReleaseInfo.tag_name -replace '^v', '' # Removes 'v' from version tag if present
            Write-Host "Latest PowerShell version is $latestVersion"
        } catch {
            Write-Host "Failed to fetch the latest PowerShell version. Error: $_" -ForegroundColor Red
        }

        # Get the currently installed PowerShell version
        $currentVersion = $PSVersionTable.PSVersion.ToString()

        # Function to compare version numbers
        function Compare-Version($Version1, $Version2) {
            return [version]$Version1 -lt [version]$Version2
        }

        $upgradePerformed = $false

        # Upgrade PowerShell if the installed version is older
        if ($UpgradePowerShell -and (Compare-Version $currentVersion $latestVersion)) {
            Write-Host "Current PowerShell version ($currentVersion) is older than the latest ($latestVersion). Upgrading..."
            Invoke-Expression "& { $(Invoke-RestMethod https://aka.ms/Install-PowerShell.ps1) } -UseMSI -Quiet"
            $upgradePerformed = $true
        } elseif ($UpgradePowerShell) {
            Write-Host "PowerShell is already up to date. Current version: $currentVersion."
        }

        # After the upgrade check
        if ($upgradePerformed) {
            $response = Read-Host "Do you want to close this PowerShell session? (Y/n)"
            if ($response -eq '' -or $response -eq 'y' -or $response -eq 'Y') {
                Write-Host "Closing PowerShell session. Please open a new session to use the updated version."
                Stop-Process -Id $PID
            } else {
                Write-Host "Please remember to manually restart PowerShell to use the updated version."
            }
        }

        # Function to check if any switch parameters were explicitly set to true
        # If none were set, it defaults to perform all actions
        # This function also removes the 'NoReboot' parameter from the list
        function Test-ParametersSet {
            param(
                [Parameter(Mandatory=$false)]
                [string[]]$params
            )

            $switchParameters = (Get-Command Frost).Parameters.Values | Where-Object { $_.ParameterType.Name -eq "SwitchParameter" } | Select-Object -ExpandProperty Name

            foreach ($param in $params) {
                if ($switchParameters -contains $param) {
                    return $false
                }
            }

            Write-Host "No specific actions requested. Defaulting to perform all actions."
            return $true
        }

        # Set all parameters to true if none were explicitly set (except NoReboot)
        # This is useful in cases where the user wants to run all functions without specifying each
        $performAllActions = Test-ParametersSet -params $PSBoundParameters.Keys

        if ($performAllActions) {
            $UpgradePowerShell = $true
            $InstallChocolatey = $true
            $InstallPrograms = $true
            $Install1Password = $true
            $InstallWedge = $true
            $InstallWSL = $true
            $InstallUbuntu = $true
            $InstallWSLAnsible = $true
            $InstallWinRM = $true
            $ConfigureWindowsTerminal = $true
            $Reboot = $true
        }

        # Function to install Chocolatey if it's not installed
        # Chocolatey is a package manager for windows
        function Install-Chocolatey {
            if (-not (Get-Command "choco" -ErrorAction SilentlyContinue)) {
                Write-Host "Installing Chocolatey..."
                Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
            } else {
                Write-Host "✅ Chocolatey is already installed." -ForegroundColor Green
            }
        }

        # Function to install basic programs using Chocolatey
        # It checks if the packages are already installed before attempting to install
        # The basic programs include git, googlechrome, gsudo, msys2, vscode
        function Install-Programs {
            $installedPackages = choco list --local-only
            $packages = @('git', 'googlechrome', 'gsudo', 'msys2', 'vscode', 'vscode-insiders')
            foreach ($package in $packages) {
                if ($installedPackages -notmatch $package) {
                    Write-Host "Installing $package..."
                    choco install $package --yes
                } else {
                    Write-Host "✅ $package is already installed." -ForegroundColor Green
                }
            }
        }

        # Function to install Wedge: https://github.com/MarcGuiselin/wedge
        function Install-Wedge {
            Write-Host "Checking if Wedge is installed..."

            # This is where you installed Wedge
            $exePath = "$env:USERPROFILE\AppData\Local\Wedge\wedge.exe"

            if (!(Test-Path $exePath)) {
                Write-Host "Wedge is not installed, installing now..."
                $URL = Get-LatestWedgeURL
                $folderPath = "$env:Temp\Wedge"
                if (!(Test-Path $folderPath)) {
                    New-Item -ItemType Directory -Path $folderPath
                }

                # Extract the version from the URL
                $version = $URL -replace '^.*\/download\/v([^\/]+)\/.*$', '$1'

                $Path = Join-Path -Path $folderPath -ChildPath "install_wedge_v$version.exe"

                try {
                    Invoke-WebRequest -Uri $URL -OutFile $Path
                } catch {
                    Write-Host "There was an error downloading Wedge: $_" -ForegroundColor Red
                    return
                }

                # Open the file explorer at the new location
                Start-Process explorer.exe -ArgumentList ("/select,$Path")

                # Instruct the user to manually start the installer
                Write-Host "Please navigate to $Path and double-click the installer to install Wedge."
            } else {
                Write-Host "✅ Wedge is already installed." -ForegroundColor Green
            }
        }

        function Get-LatestWedgeURL {
            $releases = Invoke-WebRequest -Uri https://api.github.com/repos/MarcGuiselin/wedge/releases -UseBasicParsing | ConvertFrom-Json
            $latestURL = $releases.assets.browser_download_url -match 'installer' | Select-Object -First 1
            return $latestURL
        }

        # Function to install 1Password
        function Install-1Password {
            Write-Host "Checking if 1Password is installed..."
            
            $exePath = "$env:USERPROFILE\AppData\Local\1Password\app\8\1Password.exe"
            
            if (!(Test-Path $exePath)) {
                Write-Host "1Password is not installed, installing now..."
                $URL = 'https://downloads.1password.com/win/1PasswordSetup-latest.exe'
                $Path = "$env:Temp\1PasswordSetup-latest.exe"
                
                try {
                    Invoke-WebRequest -Uri $URL -OutFile $Path
                } catch {
                    Write-Host "There was an error downloading 1Password: $_" -ForegroundColor Red
                    return
                }
        
                try {
                    Start-Process -FilePath $Path
                } catch {
                    Write-Host "There was an error installing 1Password: $_" -ForegroundColor Red
                    return
                }
        
                Remove-Item $Path -Force
            } else {
                Write-Host "✅ 1Password is already installed." -ForegroundColor Green
            }
        }

        # Function to ensure WSL is installed but don't install a default distribution
        # WSL stands for Windows Subsystem for Linux, allowing Linux distributions to run on Windows
        function Install-WSL {
            $wslStatus = wsl --status
            if ($wslStatus -match "is not installed") {
                Write-Host "Installing Windows Subsystem for Linux (WSL) components..."
                wsl --install --no-distribution
                Wait-WSLInstallation
            } else {
                Write-Host "✅ WSL is already installed" -ForegroundColor Green
            }
        }

        # Function to wait for WSL installation to complete
        # This is necessary because the installation process can take some time
        function Wait-WSLInstallation {
            Write-Host "Waiting for WSL installation to complete..."
            Start-Sleep -Seconds 30 # Adjust this value based on your system speed
        }

        # Function to check and install Ubuntu if it's not installed
        # Ubuntu is a popular Linux distribution
        function Install-Ubuntu {
            $ubuntuVersion = "Ubuntu-24.04"
            $ubuntuInstalled = (wsl -l -q) -contains $ubuntuVersion
            if (-not $ubuntuInstalled) {
                Write-Host "$ubuntuVersion is not installed. Installing without launching..."
                wsl --install -d $ubuntuVersion --no-launch
            } else {
                Write-Host "✅ $ubuntuVersion is already installed." -ForegroundColor Green
            }
        }

        function Set-WSLUser {
            $username = "paul"
            $password = "changeme"
        
            # Check if the user already exists in WSL
            $userExistsCommand = "id -u $username"
            $userExists = wsl bash -c $userExistsCommand 2>&1
        
            if ($userExists -like "*no such user*") {
                Write-Host "User $username does not exist. Creating..."
        
                # Create the user, set password, and add to sudo group
                $createUserCommand = "sudo useradd -m $username -s /bin/bash"
                $setPasswordCommand = "echo '${username}:${password}' | chpasswd"
                $addToSudoCommand = "sudo usermod -aG sudo $username"
        
                wsl bash -c $createUserCommand
                wsl bash -c $setPasswordCommand
                wsl bash -c $addToSudoCommand
        
            } else {
                Write-Host "User $username already exists. Skipping creation."
                # Optionally, you could reset the password here or ensure the user is part of the sudo group.
            }
        
            # Setting the user as default for WSL
            Write-Host "Checking if $username is the default user for WSL..."
            $checkDefaultUserCommand = "grep -q 'default=$username' /etc/wsl.conf && echo 'already set' || echo 'not set'"
            $defaultUserStatus = wsl bash -c $checkDefaultUserCommand

            if ($defaultUserStatus -eq 'not set') {
                Write-Host "Setting $username as the default user for WSL..."
                $setDefaultUserCommand = "echo -e '\n[user]\ndefault=$username' | tee -a /etc/wsl.conf"
                wsl bash -c $setDefaultUserCommand
            } else {
                Write-Host "$username is already the default user for WSL."
            }
        }

        # Function to install ansible and software-properties-common
        function Install-WSLAnsible {
            Write-Host "Installing ansible and software-properties-common..."
            $installCommand = "sudo apt-get update && sudo apt-get install software-properties-common ansible -y"
            wsl bash -c $installCommand
        }

        function Install-WinRM {
            $url = "https://raw.githubusercontent.com/ansible/ansible-documentation/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
            $file = "$env:temp\ConfigureRemotingForAnsible.ps1"
            (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
            powershell.exe -ExecutionPolicy ByPass -File $file
            Write-Host "WinRM has been installed and configured for Ansible."
        
            Get-ChildItem -Path WSMan:\localhost\Listener | Where-Object { $_.Keys -contains "Transport=HTTP" } | Remove-Item -Recurse -Force
            Write-Host "HTTP WinRM listeners have been removed."
        }

        # Function to configure Windows Terminal and install required font
        # It downloads the font and configuration from the provided URLs
        # It also checks if the font is already installed before attempting to download
        function Set-WindowsTerminalWithPowerlineFontFont {
            # Define the font and configuration URLs
            $fontUrl = "https://github.com/powerline/fonts/raw/master/RobotoMono/Roboto%20Mono%20for%20Powerline.ttf"
            $configUrl = "https://gist.githubusercontent.com/pcuci/55f634ab4c1ffc1713cf5271f4259448/raw/9febb6b1cbbf412889a76b71510e0de149a990a4/windows-terminal-settings.json"
            $localAppData = $env:LOCALAPPDATA
            $terminalSettingsPath = "$localAppData\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"
            $fontsDir = "$env:windir\Fonts"
            $fontName = "Roboto Mono for Powerline.ttf"

            # Prompt the user to override terminal settings at the beginning
            if ((Test-Path $terminalSettingsPath) -and (Read-Host "Overwrite existing Windows Terminal settings? (Y/n)") -ne 'Y') {
                Write-Host "Skipping Windows Terminal configuration and font installation..."
                return
            }

            # Check and download the font if not already installed
            if (-not (Test-Path "$fontsDir\$fontName")) {
                Write-Host "Downloading and installing the font: $fontName..."
                $fontData = Invoke-WebRequest -Uri $fontUrl -UseBasicParsing
                $fontPath = "$fontsDir\$fontName"
                [System.IO.File]::WriteAllBytes($fontPath, $fontData.Content)
                Add-Font -FontFilePath $fontPath
            } else {
                Write-Host "✅ Font '$fontName' is already installed." -ForegroundColor Green
            }

            # Download and apply Windows Terminal settings
            Write-Host "Configuring Windows Terminal with new settings..."
            Invoke-WebRequest -Uri $configUrl -OutFile $terminalSettingsPath -UseBasicParsing
        }

        # Function to add a font to the system
        # This is used by the Set-WindowsTerminalWithPowerlineFontFont function
        function Add-Font {
            param ($FontFilePath)
            $fonts = New-Object -ComObject Shell.Application
            $fontFolder = $fonts.NameSpace(0x14)
            $fontFolder.CopyHere($FontFilePath)
            Write-Host "Font installed: $FontFilePath"
        }
        # Main script execution starts here
        # It checks each parameter and performs the corresponding action if the parameter is set to true
        # It also handles the reboot logic, asking for user confirmation before rebooting
        try {
            if ($InstallChocolatey) {
                try {
                    Install-Chocolatey
                } catch {
                    Write-Host "An error occurred during Chocolatey installation: $_"
                }
            }

            if ($InstallPrograms) {
                try {
                    Install-Programs
                } catch {
                    Write-Host "An error occurred during the installation of basic programs: $_"
                }
            }

            if ($InstallWedge) {
                try {
                    Install-Wedge
                } catch {
                    Write-Host "An error occurred during Wedge installation: $_"
                }
            }

            if ($Install1Password) {
                try {
                    Install-1Password
                } catch {
                    Write-Host "An error occurred during 1Password installation: $_"
                }
            }

            if ($InstallWSL) {
                try {
                    Install-WSL
                } catch {
                    Write-Host "An error occurred during WSL installation: $_"
                }
            }

            if ($InstallUbuntu) {
                try {
                    Install-Ubuntu
                } catch {
                    Write-Host "An error occurred during Ubuntu installation: $_"
                }
            }

            if ($SetWSLUser) {
                try {
                    Set-WSLUser
                } catch {
                    Write-Host "An error occurred during WSL user setup: $_"
                }
            }

            if ($InstallWSLAnsible) {
                Install-WSLAnsible
            }

            if ($InstallWinRM) {
                try {
                    InstallAndConfigure-WinRM
                } catch {
                    Write-Host "An error occurred during WinRM installation: $_"
                }
            }

            if ($ConfigureWindowsTerminal) {
                try {
                    Set-WindowsTerminalWithPowerlineFontFont
            PowerlineFont   } catch {
                    Write-Host "An error occurred during Windows Terminal configuration: $_"
                }
            }

            # Adjusting the reboot logic to account for the NoReboot switch
            if ($Reboot -and -not $NoReboot) {
                $rebootConfirmation = Read-Host "Do you want to reboot now? (y/N)"
                if ($rebootConfirmation -eq 'y') {
                    Write-Host "Rebooting the machine..."
                    Restart-Computer
                } else {
                    Write-Host "Reboot skipped. Please remember to manually reboot your machine later."
                }
            } elseif ($NoReboot) {
                Write-Host "Reboot step has been disabled by --no-reboot option."
            }
        } catch {
        Write-Host "An unexpected error occurred during execution: $_"
        }
    }

    Export-ModuleMember -Function Frost
}

